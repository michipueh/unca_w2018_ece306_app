package xyz.pueh.app;

import android.app.Activity;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.io.IOException;

public class MainActivity extends AppCompatActivity implements ControlFragment.OnControllerInputListener, JoystickFragment.OnJoystickInputListener, ConnectFragment.OnConnectListener {
    private static final String TAG = "MainActivity";
    public static final int JOYSTICK_INDEX = 0;
    public static final int CONTROL_INDEX = 1;
    public static final int CONNECT_INDEX = 2;
    public static final int COMMAND_WAIT_TIME = 100;

    private ViewPager mViewPager;
    private BottomNavigationView mNavigation;
    private int mLastLeft = 0;
    private int mLastRight = 0;
    private boolean mLastForward = true;
    private boolean mLastHandled = false;
    private final Handler mCommandSendHandler = new Handler();

    private TCPClient2 mTCPClient = new TCPClient2();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewPager = findViewById(R.id.viewpager);
        mViewPager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager()));
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
                View v = getCurrentFocus();
                if(v == null) {
                    v = new View(getApplicationContext());
                }
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                for (int i = 0; i < mNavigation.getMenu().size(); i++) {
                    mNavigation.getMenu().getItem(i).setChecked(false);
                }
                mNavigation.getMenu().getItem(position).setChecked(true);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mNavigation = findViewById(R.id.navigation);
        mNavigation.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.navigation_joystick:
                    mViewPager.setCurrentItem(JOYSTICK_INDEX);
                    return true;
                case R.id.navigation_control:
                    mViewPager.setCurrentItem(CONTROL_INDEX);
                    return true;
                case R.id.navigation_connect:
                    mViewPager.setCurrentItem(CONNECT_INDEX);
                    return true;
            }
            return false;
        });
    }

    @Override
    public void onSendCommandClick(String command) {
        if (mTCPClient != null && command != null && !command.isEmpty()) {
            for (String cmd : command.split("&")) {
                mTCPClient.sendCommand(cmd);
            }
        }
    }

    @Override
    public void onJoystickChange(int left, int right, boolean forward) {
        Log.d(TAG, "L: " + left + " R: " + right + " F: " + forward);
        mLastHandled = false;
        mLastLeft = left;
        mLastRight = right;
        mLastForward = forward;
    }

    @Override
    public void onConnect(String ip, int port, String pin) {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setBackgroundDrawable(new ColorDrawable(getResources()
                    .getColor(R.color.colorPrimary, null)));
            getWindow().setStatusBarColor(getResources()
                    .getColor(R.color.colorPrimaryDark, null));
            actionBar.setTitle("Connecting to " + ip);
        }

        mTCPClient.connect(ip, port, pin, (status, message) -> {
            runOnUiThread(() -> {
                if (actionBar != null) {
                    switch (status) {
                        case CONNECTED:
                            actionBar.setBackgroundDrawable(new ColorDrawable(getResources()
                                    .getColor(R.color.colorPrimaryGreen, null)));
                            getWindow().setStatusBarColor(getResources()
                                    .getColor(R.color.colorPrimaryDarkGreen, null));
                            actionBar.setTitle("Connected to " + ip);
                            try {
                                mTCPClient.startSending((status1, message1) -> {
                                    Log.d(TAG, status1 + " " + message1);
                                });
                                mTCPClient.startReceiving((status2, message2) -> {
                                    Log.d(TAG, status2 + " " + message2);
                                });

                                mCommandSendHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(!mLastHandled) {
                                            mLastHandled = true;
                                            if(mLastForward) {
                                                mTCPClient.sendCommand("MFP " + mLastLeft + " " + mLastRight);
                                            } else {
                                                mTCPClient.sendCommand("MRP " + mLastLeft + " " + mLastRight);
                                            }
                                        }
                                        mCommandSendHandler.postDelayed(this, COMMAND_WAIT_TIME);
                                    }
                                }, COMMAND_WAIT_TIME);
                            } catch (IOException e) {
                                Log.e(TAG, e.toString());
                            }
                            break;
                        case NOT_CONNECTED:
                        case DISCONNECTED:
                            actionBar.setBackgroundDrawable(new ColorDrawable(getResources()
                                    .getColor(R.color.colorPrimaryRed, null)));
                            getWindow().setStatusBarColor(getResources()
                                    .getColor(R.color.colorPrimaryDarkRed, null));
                            actionBar.setTitle("Not connected");
                            mCommandSendHandler.removeCallbacksAndMessages(null);
                            break;
                    }
                }
            });
        });
    }

    @Override
    public void onDisconnect() {
        stopTCPClient();
    }

    private void stopTCPClient() {
        if (mTCPClient != null) {
            mTCPClient.disconnect();
        }
    }
}
