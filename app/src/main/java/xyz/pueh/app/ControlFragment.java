package xyz.pueh.app;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.jmedeisis.bugstick.Joystick;
import com.jmedeisis.bugstick.JoystickListener;

import java.util.ArrayList;
import java.util.List;

public class ControlFragment extends Fragment {
    private TextView mCommandView;

    private OnControllerInputListener mListener;

    public static ControlFragment newInstance() {
        return new ControlFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_control, container, false);

        mCommandView = view.findViewById(R.id.txtCommand);

        Button mSendCommandButton = view.findViewById(R.id.btnSendCommand);
        mSendCommandButton.setOnClickListener((v) -> {
            if (mCommandView.getText().length() != 0) {
                onButtonPressed(mCommandView.getText().toString());
            } else {
                mCommandView.setError("Invalid command");
            }
        });

        Button mSendLightButton = view.findViewById(R.id.btnSendLight);
        mSendLightButton.setOnClickListener((v) -> {
            onButtonPressed("LB");
        });

        Button mSendStartLineButton = view.findViewById(R.id.btnSendStartLine);
        mSendStartLineButton.setOnClickListener((v) -> {
            onButtonPressed("STARTLINE");
        });

        Button mSendStopLineButton = view.findViewById(R.id.btnSendStopLine);
        mSendStopLineButton.setOnClickListener((v) -> {
            onButtonPressed("STOPLINE");
        });

        return view;
    }

    public void onButtonPressed(String command) {
        if (mListener != null) {
            mListener.onSendCommandClick(command);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnControllerInputListener) {
            mListener = (OnControllerInputListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnControllerInputListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnControllerInputListener {
        void onSendCommandClick(String command);
    }
}