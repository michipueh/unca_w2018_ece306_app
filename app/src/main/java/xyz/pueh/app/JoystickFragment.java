package xyz.pueh.app;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jmedeisis.bugstick.Joystick;
import com.jmedeisis.bugstick.JoystickListener;

public class JoystickFragment extends Fragment {
    private int mLeft = 100;
    private int mRight = 100;
    private boolean mBackwards = false;
    private float mSpeed = 0f;

    private OnJoystickInputListener mListener;

    public static JoystickFragment newInstance() {
        return new JoystickFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_joystick, container, false);

        Button mJoystickButtonHorizontal = view.findViewById(R.id.cmdJoystickButtonHorizontal);
        mJoystickButtonHorizontal.setOnTouchListener((v, event) -> true);
        Button mJoystickButtonVertical = view.findViewById(R.id.cmdJoystickButtonVertical);
        mJoystickButtonVertical.setOnTouchListener((v, event) -> true);

        Joystick mJoystickHorizontal = view.findViewById(R.id.joystickHorizontal);
        mJoystickHorizontal.setJoystickListener(new JoystickListener() {
            @Override
            public void onDown() {
                // ..
            }

            @Override
            public void onDrag(float degrees, float offset) {
                // Right
                if(offset < 0.2f) {
                    mLeft = 100;
                    mRight = 100;
                } else if(degrees == 0f) {
                    mLeft = 100;
                    mRight = 100 - Math.round(offset * 100);
                } else {
                    mRight = 100;
                    mLeft = 100 - Math.round(offset * 100);
                }

                mRight = Math.min(100, Math.max(0, mRight));
                mLeft = Math.min(100, Math.max(0, mLeft));

                notifyJoystickChange();
            }

            @Override
            public void onUp() {
                mRight = 100;
                mLeft = 100;
                notifyJoystickChange();
            }
        });

        Joystick mJoystickVertical = view.findViewById(R.id.joystickVertical);
        mJoystickVertical.setJoystickListener(new JoystickListener() {
            @Override
            public void onDown() {
                // ..
            }

            @Override
            public void onDrag(float degrees, float offset) {
                mBackwards = degrees < 0;
                if (offset < 0.3f) {
                    offset = 0;
                } else if (offset > 1f) {
                    offset = 1f;
                }
                mSpeed = Math.min(1f, Math.max(0f, offset));
                notifyJoystickChange();
            }

            @Override
            public void onUp() {
                mBackwards = false;
                mSpeed = 0f;
                notifyJoystickChange();
            }
        });

        return view;
    }

    private void notifyJoystickChange() {
        if (mListener != null)
            mListener.onJoystickChange((int) (mLeft * mSpeed), (int) (mRight * mSpeed), !mBackwards);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnJoystickInputListener) {
            mListener = (OnJoystickInputListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnJoystickInputListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnJoystickInputListener {
        void onJoystickChange(int left, int right, boolean forward);
    }
}