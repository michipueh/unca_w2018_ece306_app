package xyz.pueh.app;

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class TCPClient2 {
    private static String TAG = "TCPClient2";

    private Socket connectionSocket;

    //Runnables for sending and receiving data
    private SendRunnable sendRunnable;
    //Threads to execute the Runnables above
    private Thread sendThread;
    private Thread receiveThread;

    private String mIP = "192.168.0.2";
    private int mPort = 1234;
    private String mPin = "1234";
    private ConnectListener mConnectListener;
    private boolean mWasConnected = false;


    /**
     * Returns true if TCPClient is connected, else false
     *
     * @return Boolean
     */
    public boolean isConnected() {
        return connectionSocket != null && connectionSocket.isConnected() && !connectionSocket.isClosed();
    }

    /**
     * Open connection to server
     */
    public void connect(String ip, int port, String pin, ConnectListener l) {
        mIP = ip;
        mPort = port;
        mPin = pin;
        mConnectListener = l;
        mWasConnected = true;
        new Thread(new ConnectRunnable(l)).start();
    }

    public void connect() {
        if (mWasConnected)
            connect(mIP, mPort, mPin, mConnectListener);
    }

    /**
     * Close connection to server
     */
    public void disconnect() {
        stopThreads();

        if (connectionSocket != null) {
            try {
                connectionSocket.close();
                Log.d(TAG, "Disconnected!");
            } catch (IOException e) {
                Log.e(TAG, e.toString());
            }
            if(mConnectListener != null) {
                mConnectListener.onConnected(ConnectListener.Status.DISCONNECTED, "Disconnected");
            }
        }
    }

    public boolean sendCommand(String command) {
        return isConnected() && sendRunnable != null &&
                sendRunnable.send("$" + mPin + " " + command + "!");
    }

    private void stopThreads() {
        if (receiveThread != null)
            receiveThread.interrupt();

        if (sendThread != null)
            sendThread.interrupt();
    }

    public void startSending(SendListener l) throws IOException {
        if (isConnected()) {
            sendRunnable = new SendRunnable(connectionSocket, l);
            sendThread = new Thread(sendRunnable);
            sendThread.start();
        }
    }

    public void startReceiving(ReceiveListener l) throws IOException {
        if (isConnected()) {
            ReceiveRunnable receiveRunnable = new ReceiveRunnable(connectionSocket, l);
            receiveThread = new Thread(receiveRunnable);
            receiveThread.start();
        }
    }

    public class ReceiveRunnable implements Runnable {
        private BufferedReader mIn;
        private String mServerMessage;
        private ReceiveListener mListener;

        public ReceiveRunnable(Socket server, ReceiveListener l) throws IOException {
            mIn = new BufferedReader(new InputStreamReader(server.getInputStream()));
            mListener = l;
        }

        @Override
        public void run() {
            Log.d(TAG, "Receiving started");
            if (mListener != null)
                mListener.onReceived(ReceiveListener.Status.STARTED, "Started receiving.");

            while (!Thread.currentThread().isInterrupted() && isConnected()) {
                try {
                    mServerMessage = mIn.readLine();

                    if (mServerMessage != null && mListener != null) {
                        mListener.onReceived(ReceiveListener.Status.RECEIVED, mServerMessage);
                    }
                } catch (IOException e) {
                    Log.e(TAG, e.toString());
                    if (mListener != null)
                        mListener.onReceived(ReceiveListener.Status.NOT_RECEIVED, e.toString());
                    disconnect(); //Gets stuck in a loop if we don't call this on error!
                }
            }
            Log.d(TAG, "Receiving stopped");
            if (mListener != null)
                mListener.onReceived(ReceiveListener.Status.STOPPED, "Stopped receiving.");
        }

    }

    public interface ReceiveListener {
        enum Status {
            RECEIVED, NOT_RECEIVED, STOPPED, STARTED
        }

        void onReceived(Status status, String message);
    }

    public class SendRunnable implements Runnable {
        private BufferedWriter mOut;
        private final Queue<String> mCommandQueue = new ArrayBlockingQueue<>(20);
        private SendListener mListener;

        public SendRunnable(Socket server, SendListener l) throws IOException {
            this.mOut =
                    new BufferedWriter(
                            new OutputStreamWriter(server.getOutputStream()));
            this.mListener = l;
        }

        public boolean send(String command) {
            return command != null && mCommandQueue.offer(command);
        }

        @Override
        public void run() {
            Log.d(TAG, "Sending started");
            if (mListener != null)
                mListener.onSent(SendListener.Status.STARTED, "Sending started.");
            while (!Thread.currentThread().isInterrupted() && isConnected()) {
                String command;
                if ((command = mCommandQueue.poll()) != null) {
                    try {
                        mOut.write(command);
                        mOut.write('\r');
                        mOut.flush();
                        Log.d(TAG, "Command has been sent! ");
                        if (mListener != null)
                            mListener.onSent(SendListener.Status.SENT, "Command '" + command + "' has been sent!");
                    } catch (IOException e) {
                        Log.e(TAG, "Command could not be sent! " + e.toString());
                        if (mListener != null)
                            mListener.onSent(SendListener.Status.STOPPED, "Command '" + command + "' not sent! " + e.toString());
                        Thread.currentThread().interrupt();
                    }
                    //if (!receiveThreadRunning)
                    //    startReceiving(); //Start the receiving thread if it's not already running
                }
            }
            if (mListener != null)
                mListener.onSent(SendListener.Status.STOPPED, "Sending stopped.");
            Log.d(TAG, "Sending stopped");
        }
    }

    public interface SendListener {
        enum Status {
            SENT, NOT_SENT, STOPPED, STARTED
        }

        void onSent(Status status, String message);
    }

    public class ConnectRunnable implements Runnable {
        ConnectListener mListener;

        ConnectRunnable(ConnectListener l) {
            this.mListener = l;
        }

        public void run() {
            try {
                Log.d(TAG, "Connecting...");
                InetAddress serverAddr = InetAddress.getByName(mIP);

                connectionSocket = new Socket();
                connectionSocket.connect(new InetSocketAddress(serverAddr, mPort), 5000);
                if (mListener != null)
                    mListener.onConnected(ConnectListener.Status.CONNECTED, "Connected!");
                Log.d(TAG, "Connected!");
            } catch (Exception e) {
                if (mListener != null)
                    mListener.onConnected(ConnectListener.Status.NOT_CONNECTED, e.toString());
                Log.e(TAG, e.toString());
            }
            Log.d(TAG, "Connetion thread stopped");
        }
    }

    public interface ConnectListener {
        enum Status {
            CONNECTED, NOT_CONNECTED, DISCONNECTED
        }

        void onConnected(Status status, String message);
    }
}