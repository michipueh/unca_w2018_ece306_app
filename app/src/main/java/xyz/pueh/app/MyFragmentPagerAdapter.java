package xyz.pueh.app;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import static xyz.pueh.app.MainActivity.CONNECT_INDEX;
import static xyz.pueh.app.MainActivity.CONTROL_INDEX;
import static xyz.pueh.app.MainActivity.JOYSTICK_INDEX;

public class MyFragmentPagerAdapter extends FragmentPagerAdapter {
    public MyFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        switch (i) {
            case CONTROL_INDEX:
                return ControlFragment.newInstance();
            case CONNECT_INDEX:
                return ConnectFragment.newInstance();
            case JOYSTICK_INDEX:
                return JoystickFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 3;
    }
}
