package xyz.pueh.app;

import android.app.AlertDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ConnectFragment extends Fragment {
    private EditText mPinInput;
    private EditText mPortInput;
    private EditText mIPInput;

    private OnConnectListener mListener;

    public static ConnectFragment newInstance() {
        return new ConnectFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_connect, container, false);

        Button mConnectButton = view.findViewById(R.id.btnConnect);
        Button mDisconnectButton = view.findViewById(R.id.btnDisconnect);
        mPinInput = view.findViewById(R.id.txtPin);
        mPortInput = view.findViewById(R.id.txtPort);
        mIPInput = view.findViewById(R.id.txtIP);

        SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
        mIPInput.setText(sharedPref.getString("saved_ip", ""));
        mPortInput.setText(sharedPref.getString("saved_port", ""));
        mPinInput.setText(sharedPref.getString("saved_pin", ""));

        mPinInput.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                handleConnect();
                return true;
            }
            return false;
        });

        mConnectButton.setOnClickListener(v -> handleConnect());
        mDisconnectButton.setOnClickListener(v -> handleDisconnect());

        return view;
    }

    private void handleConnect() {
        List<String> errors = new ArrayList<>();

        int port = 0;
        try {
            port = Integer.parseInt(mPortInput.getText().toString());
        } catch (NumberFormatException e) {
            errors.add("Invalid Port was entered!");
        }

        String ip = mIPInput.getText().toString();
        if (!Patterns.IP_ADDRESS.matcher(ip).matches()) {
            errors.add("Invalid IP-Address format.");
        }

        String pin = mPinInput.getText().toString();
        if (pin.length() != 4) {
            errors.add("Pin must be length 4.");
        }

        if (mListener != null && errors.isEmpty()) {
            SharedPreferences.Editor sharedPrefEditor = getActivity().getPreferences(Context.MODE_PRIVATE).edit();
            sharedPrefEditor.putString("saved_ip", ip);
            sharedPrefEditor.putString("saved_port", String.valueOf(port));
            sharedPrefEditor.putString("saved_pin", pin);
            sharedPrefEditor.apply();

            mListener.onConnect(ip, port, pin);
        } else {
            new AlertDialog.Builder(getContext())
                    .setMessage(String.join(System.lineSeparator(), errors))
                    .setTitle("Input Error")
                    .show();
        }
    }

    private void handleDisconnect() {
        if (mListener != null)
            mListener.onDisconnect();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnConnectListener) {
            mListener = (OnConnectListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnConnectListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnConnectListener {
        void onConnect(String ip, int port, String pin);

        void onDisconnect();
    }

}
